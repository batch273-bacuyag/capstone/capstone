/*console.log("hello")*/

class Customer {
  	constructor(email) {
	    this.email = email;
	    this.cart = new Cart();
	    this.orders = [];
  	}

  	checkOut() {
    	if (this.cart.isEmpty()) {
      		console.log('Cart is empty');
      		return;
    }

    this.cart.computeTotal();

    const order = {
      	products: this.cart.getContents(),
      	totalAmount: this.cart.totalAmount
    };

    this.orders.push(order);
    this.cart.clearCartContents();
    console.log('Order placed:', order);
  	}	
}

class Cart {
  	constructor() {
    	this.contents = [];
    	this.totalAmount = 0;
  	}

  	addToCart(product, quantity) {
    	this.contents.push({ product, quantity });
  	}

  	showCartContents() {
    	console.log('Cart contents:', this.contents);
  	}

  	updateProductQuantity(name, newQuantity) {
    	const productIndex = this.contents.findIndex(item => item.product.name === name);
    	if (productIndex === -1) {
      		console.log('Product not found in cart');
      		return;
    	}

    	this.contents[productIndex].quantity = newQuantity;
  	}

  	clearCartContents() {
    	this.contents = [];
    	this.totalAmount = 0;
  	}

  	computeTotal() {
    	let total = 0;
    	for (const item of this.contents) {
      	total += item.product.price * item.quantity;
    	}
    	this.totalAmount = total;
  	}

  	isEmpty() {
    	return this.contents.length === 0;
  	}

  	getContents() {
    	return this.contents;
  	}
}

class Product {
  	constructor(name, price, isActive = true) {
    	this.name = name;
    	this.price = price;
    	this.isActive = isActive;
  	}

  	archive() {
    	if (this.isActive) {
      	this.isActive = false;
    	}
  	}

  	updatePrice(newPrice) {
    	this.price = newPrice;
  }
}


